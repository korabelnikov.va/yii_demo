<?php

namespace app\assets;

use yii\web\AssetBundle;

class CatalogAsset extends AssetBundle
{
    public $sourcePath = '@app/catalog_resources';
    public $js = [
        'js/core_currency.min.js',
        'js/core_money_editor.map.js',
        'js/core_money_editor.min.js',
        'js/not_existed_script.js'
    ];
    public $css = [
        'css/template_styles.css',
        'css/colors.min.css',
        'css/not_existed_style.css'
    ];
    // public $publishOptions = [
    //     'only' => [
    //         'css/*',
    //     ]
    // ];
}