<?php
use yii\helpers\Html;
use app\assets\CatalogAsset;

// var_dump(
//     Yii::getAlias('@yii'),
//     Yii::getAlias('@app'),
//     Yii::getAlias('@runtime'),
//     Yii::getAlias('@vendor'),
//     Yii::getAlias('@webroot'),
//     Yii::getAlias('@web'),
// );die();


// var_dump(
    CatalogAsset::register($this)
// );

/* @var $this yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <header>
        Хэдер шаблона для каталога
        <?php if (isset($this->blocks['block1'])): ?>
            <?= $this->blocks['block1'] ?>
        <?php else: ?>
            <p>... контент по умолчанию для блока в хэдере ...</p>
        <?php endif; ?>
    </header>
    <?= $content ?>
    <footer>Моя компания &copy; 2014</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>