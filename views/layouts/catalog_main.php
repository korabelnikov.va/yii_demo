<?php 
use yii\web\View;

$this->beginContent('@app/views/layouts/catalog.php');
$this->registerJs(
    "$('h1').on('click', function() { alert('H1 clicked!'); });",
    View::POS_LOAD,
    'my-h1-handler'
);

$this->registerJs(
    "console.log('message from head tag!');",
    View::POS_HEAD,
    'my-h1-handler'
);
$this->registerCss(".red-h3 { color: #f00; }");


$options = [
    'appName' => Yii::$app->name,
    'baseUrl' => Yii::$app->request->baseUrl,
    'language' => Yii::$app->language,
];
$this->registerJs(
    "var yiiOptions = ".\yii\helpers\Json::htmlEncode($options).";",
    View::POS_HEAD,
    'yiiOptions'
);
?>
<h1>Заголовок для главной страницы каталога</h1>

<?php echo $content ?>


<h3 class="red-h3">Надпись перед футером</h3>

<?php $this->endContent(); ?>