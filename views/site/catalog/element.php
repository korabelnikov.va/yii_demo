<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use app\components\BreadcrumbWidget;
use yii\widgets\DetailView;

$this->title = 'Catalog';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $sectionCode;
$this->params['breadcrumbs'][] = $elementCode;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the Catalog page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>

    <h2><?=$elementEntity->name?></h2>

    <p><?=$elementEntity->description?></p>

    <p>
        Custom breadcrumbs:
        <?=BreadcrumbWidget::widget(
            [
                'chainList' => [
                    $rootSection => "Каталог",
                    $sectionCode => $sectionCode,
                    $elementCode => $elementCode
                ]
            ]
        )?>
    </p>


    <?=DetailView::widget([
            'model' => $elementEntity,
            'attributes' => [
                'id',
                'name',
                'description'
            ],
        ]);
    ?>
</div>