<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\BreadcrumbWidget;

$this->title = 'Catalog';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $sectionCode;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the Catalog page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>

    <ul>
        <?php foreach ($elementsList as $element): ?>
            <li><a href="<?=Url::to() . "/" . $element->code?>"><?=$element->name;?></a></li>
        <?php endforeach; ?>
    </ul>

    <p>
        Custom breadcrumbs:
        <?=BreadcrumbWidget::widget(
            [
                'chainList' => [
                    $rootSection => "Каталог",
                    $sectionCode => $sectionCode

                ]
            ]
        )?>
    </p>
</div>


<?php $this->beginBlock('block1'); ?>

<p>Количество элементов в списке для вывода в хэдере: <?php echo count($elementsList) ?>. Подсчитано и задано в шаблоне списка элементов</p>

<?php $this->endBlock(); ?>


<?php $this->registerMetaTag(['name' => 'my-custom-meta-tag', 'content' => 'yii, framework, php'], 'my-custom-meta-tag'); ?>
<?php $this->registerMetaTag(['name' => 'my-custom-meta-tag', 'content' => 'yii, framework, php rewrite previous value'], 'my-custom-meta-tag'); ?>

<?php $this->registerLinkTag([
    'title' => 'Сводка новостей по Yii',
    'rel' => 'alternate',
    'type' => 'application/rss+xml',
    'href' => 'https://www.yiiframework.com/rss.xml/',
]);  ?>
