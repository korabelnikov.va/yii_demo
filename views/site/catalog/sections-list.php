<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\BreadcrumbWidget;
use yii\widgets\LinkPager;

$this->title = 'Catalog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the Catalog page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>

    <ul>
        <?php foreach ($sectionsList as $section): ?>
            <li><a href="<?=Url::to() . "/" . $section["code"]?>"><?=$section["name"];?></a></li>
        <?php endforeach; ?>
    </ul>

    <p>
        Custom breadcrumbs:
        <?=BreadcrumbWidget::widget(
            [
                'chainList' => [
                    $rootSection => "Каталог"
                ],
            ]
        )?>
    </p>

    <p>Pager:</p>
    <?php
        echo LinkPager::widget(
            [
                'pagination' => $pages,
            ]
        );
    ?>
</div>