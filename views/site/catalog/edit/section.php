<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\Section $section */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;


?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($section, 'name')->textInput() ?>

    <?= $form->field($section, 'code')->textInput(["value" => $sectionCode]); ?>

    <?= $form->field($section, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($section, 'active')->checkbox() ?>
    
    <div class="form-group">
        <?= Html::submitButton('Submit') ?>
    </div>

<?php ActiveForm::end(); ?>

