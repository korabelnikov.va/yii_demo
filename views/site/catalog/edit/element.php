<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\Element $element */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;


?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($element, 'name')->textInput() ?>

    <?
        if($createNewElementForm)
        {
            echo $form->field($element, 'code')->hiddenInput(["value" => $elementCode])->label(false);
        }
        else
        {
            echo $form->field($element, 'code')->textInput(["value" => $elementCode]);
        }
    ?>

    <input type="hidden" name="code" value="<?=$elementCode?>">

    <?= $form->field($element, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($element, 'active')->checkbox() ?>

    <?= $form->field($element, 'sections')->checkboxList($allSections); ?>
    
    <div class="form-group">
        <?= Html::submitButton('Submit') ?>
    </div>

<?php ActiveForm::end(); ?>

