<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class BreadcrumbWidget extends Widget
{
    public $chainList = [];
    public $rootSection;

    public function run()
    {
        return $this->render(
            'breadcrumb',
            [
                "chainList" => $this->chainList
            ]
        );
    }
}