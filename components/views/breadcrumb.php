<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;

// var_dump($rootSection);die();
?>

<ul>
    <?php
        $link = "";
        $first = true;
    ?>
    <?php foreach ($chainList as $chainElementCode => $chainElementDescription): ?>
        <?php
            $link .= (($first)? "": "/") . $chainElementCode;
            $first = false;
        ?>
        <li><a href="<?=$link?>"><?=$chainElementDescription;?></a></li>
    <?php endforeach; ?>
</ul>