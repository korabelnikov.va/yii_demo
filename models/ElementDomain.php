<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ElementDomain extends Model
{
    public $name;
    public $code;
    public $description;
    public $active;
    private $_sectionsList;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['name', 'code'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['active'], 'boolean'],
            ['sectionsList', 'each', 'rule' => ['integer']]
        ];
    }

    public function getSectionsList()
    {
        return $this->_sectionsList;
    }

    public function setSectionsList($list)
    {
        $this->_sectionsList = (is_array($list)? $list: []);
    }
}