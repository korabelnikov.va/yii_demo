<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;

/**
 * Catalog is the model behind the catalog page.
 */
class Catalog extends Model
{
    const EVENT_TEST = 'test';


    public function getSectionsCount() :int
    {
        return Section::find()->count();
    }

    public function getSectionsList(Pagination $pagination = null) :array
    {
        $this->trigger(self::EVENT_TEST);
        return (empty($pagination))? Section::find()->all():
            Section::find()
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
    }

    public function getElementsForSection($sectionCode) :array
    {
        // $section = Section::findOne(["code" => $sectionCode, "active" => 1]);

        // $relations = SectionToElementRelation::findAll(["section_id" => $section->id]);

        // $elementsList = [];
        // foreach ($relations as $relation)
        // {
        //     $element = Element::findOne(["id" => $relation->element_id, "active" => 1]);
        //     if (!empty($element)) $elementsList[] = $element;
        // }



        $elementsList = [];

        $section = Section::findOne(["code" => $sectionCode, "active" => 1]);
        foreach($section->elements as $element)
        {
            $elementsList[] = $element;
        }

        return $elementsList;
    }

    public function getElement($sectionCode, $elementCode) 
    {
        return Element::findOne(["code" => $elementCode, "active" => 1]);
    }
}