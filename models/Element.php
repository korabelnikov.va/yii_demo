<?php

namespace app\models;

use yii\db\ActiveRecord;

class Element extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const GET_VALIDATION_RULES = "GET_VALIDATION_RULES";
    
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return '{{elements}}';
    }

    public function rules()
    {
        $this->trigger(self::GET_VALIDATION_RULES);
        return [
            [['code'], 'required'],
            [['name', 'code'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['active'], 'boolean']
        ];
    }

    public function init()
    {
        $this->on(
            self::EVENT_AFTER_VALIDATE,
            function($event)
            {
                // Какой-нибудь обработчик
                // var_dump($event);
                // die("1111111111111");
            }
        );

        $this->on(
            self::GET_VALIDATION_RULES,
            function($event)
            {
                // Какой-нибудь обработчик
                // var_dump($event);
                // die("1111111111111");
            }
        );
    }

    public function getSections()
    {
        return $this->hasMany(
            Section::class,
            [
                'id' => 'section_id'
            ]
        )->viaTable(
            'sections_elements_relation',
            [
                'element_id' => 'id'
            ]
        );
    }
}