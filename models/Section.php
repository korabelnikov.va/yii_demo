<?php

namespace app\models;

use yii\db\ActiveRecord;

class Section extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return '{{sections}}';
    }

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['name', 'code'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['active'], 'boolean']
        ];
    }

    public function getElements()
    {
        return $this->hasMany(
            Element::class,
            [
                'id' => 'element_id'
            ]
        )->viaTable(
            'sections_elements_relation',
            [
                'section_id' => 'id'
            ]
        );
    }
}