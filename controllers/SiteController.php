<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\base\View;
use yii\base\Theme;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Catalog;
use app\models\Element;
use app\models\Section;
use app\models\SectionToElementRelation;
use app\models\ElementDomain;
use app\models\MyUserIdentity;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', "my-user-info"],
                'rules' => [
                    // [
                    //     'actions' => ['logout'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                    [
                        'actions' => ["my-user-info"],
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        // Yii::$app->getView()->theme = new Theme([
        //     'basePath' => '@app/themes/basic',
        //     'baseUrl' => '@web/themes/basic',
        //     'pathMap' => [
        //         '@app/views' => '@app/themes/basic',
        //     ],
        // ]);
        return $this->render('about');
    }

    /**
     * Displays catalog page.
     *
     * @return string
     */
    public function actionCatalog()
    {
        \Yii::$app->view->on(View::EVENT_AFTER_RENDER, function ($event) {
            echo $event->data;
            // $event->isValid = false;
            // $event->output="test vedita";
        });

        $this->layout = 'catalog_main';
        Yii::info('Открытие страницы со списком разделов');
        \Yii::beginProfile('block1');
        // $catalogModel = new Catalog();
        $catalogModel = Yii::$app->catalog;
        // var_dump($catalogModel);
        // var_dump(new $catalogModel);
        // var_dump(new ($catalogModel::class));die();
        // $catalogModel->on('test', function ($event) {die("555");});
        $pagination = new Pagination(
            [
                'totalCount' => $catalogModel->getSectionsCount(),
                // "limit" => 1,
                // "offset" => 1,
                // "page" => 3,
                "pageSize" => 2
            ]
        );
        $sectionsList = $catalogModel->getSectionsList($pagination);
        \Yii::endProfile('block1');
        return $this->render('catalog/sections-list', [
            'model' => $catalogModel,
            'sectionsList' => $sectionsList,
            'rootSection' => Url::toRoute('site/catalog'),
            'pages' => $pagination,
        ]);
    }

    /**
     * Displays catalog page.
     *
     * @return string
     */
    public function actionSection($section)
    {
        $this->layout = 'catalog';
        $catalogModel = new Catalog();
        $elementsList = $catalogModel->getElementsForSection($section);
        return $this->render('catalog/elements-list', [
            'model' => $catalogModel,
            'elementsList' => $elementsList,
            'sectionCode' => $section,
            'rootSection' => Url::toRoute('site/catalog')
        ]);
    }


    /**
     * Displays catalog page.
     *
     * @return string
     */
    public function actionElement($sectionCode, $elementCode)
    {
        $this->layout = 'catalog';
        $catalogModel = new Catalog();
        $elementEntity = $catalogModel->getElement($sectionCode, $elementCode);
        return $this->render('catalog/element', [
            'model' => $catalogModel,
            'elementEntity' => $elementEntity,
            'sectionCode' => $sectionCode,
            'elementCode' => $elementCode,
            'rootSection' => Url::toRoute('site/catalog')
        ]);
    }

    /**
     * Displays catalog page.
     *
     * @return string
     */
    public function actionEditElementTest($elementCode)
    {
        // $data1 = [
        //     'description' => 'Element #2 description',
        //     'code' => 'element_1',
        //     'active' => 1,
        //     'name' => "element #1"
        // ];
        // $catalogModel = new Catalog();
        // $elementEntity = $catalogModel->getElement("", "element_1");

        // var_dump($elementEntity->load($data1, ''));
        // var_dump($elementEntity->save());
        // var_dump($elementEntity->getErrors());
        // var_dump($elementEntity->getAttributes());

        // die();

        $catalogModel = new Catalog();
        $elementEntity = $catalogModel->getElement("", $elementCode);
        $element = !empty($elementEntity)? $elementEntity: new Element();



        if (!empty(Yii::$app->request->post()))
        {
            $element->load(Yii::$app->request->post());
            $element->save();
            // var_dump($element->getErrors());
            // var_dump($element->getAttributes());
            // die();
        }

        return $this->render('catalog/edit/element', [
            'element' => $element
        ]);
    }


    public function actionEditElement($elementCode)
    {
        // $data1 = [
        //     'description' => 'Element #2 description',
        //     'code' => 'element_1',
        //     'active' => 1,
        //     'name' => "element #1"
        // ];
        // $catalogModel = new Catalog();
        // $elementEntity = $catalogModel->getElement("", "element_1");

        // var_dump($elementEntity->load($data1, ''));
        // var_dump($elementEntity->save());
        // var_dump($elementEntity->getErrors());
        // var_dump($elementEntity->getAttributes());

        // die();

        $catalogModel = new Catalog();

        if (!empty(Yii::$app->request->post()))
        {
            $element = Element::findOne(["code" => $elementCode, "active" => 1]);
            if(empty($element)) $element = new Element();


            if ($element->load(Yii::$app->request->post()))
            {
                foreach(Yii::$app->request->post()['Element']['sections'] as $secionId)
                {
                    $section = Section::find()->where(['id' => $secionId])->limit(1)->one();
                    if (!empty($section)) $element->link('sections', $section);
                }
                $element->save();
            }




            // $obElDomain = new ElementDomain();
            // $obElDomain->load(Yii::$app->request->post());
            // $elementEntity = Element::findOne(["code" => $elementCode, "active" => 1]);
            // $element = !empty($elementEntity)? $elementEntity: new Element();
            // $element->name = $obElDomain->name;
            // $element->code = $obElDomain->code;
            // $element->description = $obElDomain->description;
            // $element->active = $obElDomain->active;
            // $element->save();

            // SectionToElementRelation::deleteAll(["element_id" => $element->id]);

            // // var_dump($obElDomain->sectionsList);
            // // var_dump(Yii::$app->request->post());
            // // die();


            // if(is_array($obElDomain->sectionsList))
            // {
            //     foreach($obElDomain->sectionsList as $section_id)
            //     {
            //         $relation = new SectionToElementRelation();
            //         $relation->section_id = $section_id;
            //         $relation->element_id = $element->id;
            //         $relation->save();

            //     }
            // }

            // var_dump($obElDomain->getErrors());
            // var_dump($obElDomain->getAttributes());
            // die();
        }

        
        $elementEntity = Element::findOne(["code" => $elementCode]);
        $element = !empty($elementEntity)? $elementEntity: new Element();
        $sectionsList = $catalogModel->getSectionsList();
        $arAllSections = [];
        foreach ($sectionsList as $section)
        {
            $arAllSections[$section->id] = $section->name;
        }

        // $obElDomain = new ElementDomain();
        // $obElDomain->name = $element->name;
        // $obElDomain->code = $element->code;
        // $obElDomain->description = $element->description;
        // $obElDomain->active = $element->active;

        // $obElDomain->sectionsList = [];

        // $relationsList = SectionToElementRelation::findAll(["element_id" => $element->id]);

        // $arSectionsList = [];
        // foreach($relationsList as $relation)
        // {
        //     $arSectionsList[] = $relation->section_id;
        // }

        // $obElDomain->sectionsList = $arSectionsList;


        return $this->render('catalog/edit/element', [
            'element' => $element,
            'allSections' => $arAllSections,
            'elementCode' => $elementCode,
            'createNewElementForm' => empty($element->id)
        ]);
    }

    /**
     * Displays catalog page.
     *
     * @return string
     */
    public function actionEditSection($sectionCode)
    {
        $sectionEntity = Section::findOne(["code" => $sectionCode]);
        $section = !empty($sectionEntity)? $sectionEntity: new Section();



        if (!empty(Yii::$app->request->post()))
        {
            $section->load(Yii::$app->request->post());
            $section->save();
            // var_dump($element->getErrors());
            // var_dump($element->getAttributes());
            // die();
        }

        return $this->render('catalog/edit/section', [
            'section' => $section,
            'sectionCode' => $sectionCode
        ]);
    }

    public function actionMyLogin($login)
    {
        // var_dump($login);die("11111111");
        $identity = MyUserIdentity::findOne(['login' => $login]);
        // $identity = new MyUserIdentity();
        // $identity->login = "test";
        // $identity->password = "test";
        // $identity->save();

        // логиним пользователя
        Yii::$app->user->login($identity);

        return null;
    }

    public function actionMyUserInfo()
    {
        var_dump(Yii::$app->user->isGuest);
    }

    public function actionMyLogout()
    {
        Yii::$app->user->logout();
        return null;
    }

    public function actionTestTableRelation()
    {
        $element = Element::findOne(["code" => "element_1", "active" => 1]);

        var_dump($element->sections);
        foreach($element->sections as $test)
        {
            var_dump($test);
        }


        $section = Section::findOne(["code" => "section_1", "active" => 1]);
        var_dump($section->elements);
        foreach($section->elements as $test)
        {
            var_dump($test);
        }
        die();
    }

    public function actionListView()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Section::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);


        return $this->render(
            'list-view',
            [
                'dataProvider' => $dataProvider
            ]
        );
        // "listview"
    }

    public function actionTesttt()
    {
        // $this->layout = 'catalog';
        // Yii::$app->layout = 'catalog';
        $this->layout = false;
        // return "test vedita 2024";
        // return $this->render('index')
        var_dump("1111111111111");
        var_dump($this->render('index'));
        die("2222222");
    }

    public function actionTestAliases()
    {
        Yii::setAlias('@underDocumentRoot', dirname($_SERVER['DOCUMENT_ROOT']));
        Yii::setAlias('@underDocumentRoot/aaa.txt', dirname($_SERVER['DOCUMENT_ROOT']).'/bbb.txt');
        echo Yii::getAlias('@underDocumentRoot/aaa.txt');die();
    }
}
