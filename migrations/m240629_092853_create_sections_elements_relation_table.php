<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sections_elements_relation}}`.
 */
class m240629_092853_create_sections_elements_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sections_elements_relation}}', [
            'id' => $this->primaryKey(),
            'section_id' => $this->integer()->notNull(),
            'element_id' => $this->integer()->notNull()
        ]);

        // add foreign key for table `sections`
        $this->addForeignKey(
            'fk-sections_elements_relation-section_id',
            'sections_elements_relation',
            'section_id',
            'sections',
            'id',
            null
        );

        // add foreign key for table `elements`
        $this->addForeignKey(
            'fk-sections_elements_relation-element_id',
            'sections_elements_relation',
            'element_id',
            'elements',
            'id',
            null
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sections_elements_relation}}');
    }
}
